projects = [];
categories = [];
selectedProject = {};
selectedCategory = {};
user_id = "";
days = [];
angular.module('Timesheet.controllers', ['mwl.calendar', 'ionic-datepicker', 'Timesheet.services', 'base64']).factory('CommonCode', function($window, $ionicPopup, $rootScope) {
	var root = {};

	root.showHeader = function(title, display) {
		var headerVisibility = angular.element(document.getElementById('section_header')).scope();
		headerVisibility.$apply(function() {
			headerVisibility.showHeader = display;
		});
		var sectionTitle = angular.element(document.getElementById('title')).scope();
		sectionTitle.$apply(function() {
			sectionTitle.headerTitle = title;
		});

	};
	root.backButtonVisibility = function(show) {
		var backButton = angular.element(document.getElementById('backButton')).scope();
		backButton.$apply(function() {
			backButton.backVisibility = show;
		});
	};
	root.showLeaveButton = function(showBtn){
		var leaveBtnVisibility =  angular.element(document.getElementById('applyLeaveBtn')).scope();
		leaveBtnVisibility.$apply(function(){
			leaveBtnVisibility.showBtn = showBtn;
		});
	};
	root.logoutButtonVisibility = function(visible) {
		var logoutButton = angular.element(document.getElementById('logout')).scope();

		logoutButton.$apply(function() {
			$rootScope.logoutVisibility = visible;
		});
	};
	root.showAlert = function(message) {
		var alertPopup = $ionicPopup.alert({
			title : 'Timesheet',
			template : message
		});
		alertPopup.then(function(res) {
			/*
			 * Any thing which need to be performed after clicking OK button on alert
			 */
		});
	};

	return root;

}).controller('CommonCtrl', function($scope, $rootScope, $ionicPopup, $ionicHistory,$ionicLoading, Network, CommonCode,$window) {
	var weekDaysList = ["Sun", "Mon", "Tue", "Wed", "thu", "Fri", "Sat"];
	var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	$scope.logout = function() {

		var confirmPopup = $ionicPopup.confirm({
			title : 'Timesheet',
			template : "Are you sure you want to logout?"
		});
		confirmPopup.then(function(res) {
			if (res) {
				console.log('Yes');
				projects = [];
				categories = [];
				selectedProject = {};
				selectedCategory = {};
				localStorage.removeItem('user');
				location.href = '#/login';

			} else {
				console.log('No');
			}
		});
	};
	$scope.back = function() {
		$ionicHistory.goBack();
	};
	$scope.goHome = function(){
		location.href = '#/calendar';
	};
	$scope.datepickerObject = {
		titleLabel : 'Select date', //Optional
		todayLabel : 'Today', //Optional
		closeLabel : 'Close', //Optional
		setLabel : 'Apply leave', //Optional
		setButtonType : 'button-assertive', //Optional
		todayButtonType : 'button-assertive', //Optional
		closeButtonType : 'button-assertive', //Optional
		inputDate : new Date(), //Optional
		mondayFirst : true, //Optional
		disabledDates : '', //Optional
		weekDaysList : weekDaysList, //Optional
		monthList : monthList, //Optional
		templateType : 'popup', //Optional
		showTodayButton : 'false', //Optional

		modalHeaderColor : 'bar-positive', //Optional
		modalFooterColor : 'bar-positive', //Optional
		from : new Date(2015, 0, 1), //Optional
		to : new Date(), //Optional
		callback : function(val) {//Mandatory
			datePickerCallback(val);
			// var previousDate = $scope.datepickerObject.inputDate;
			// if (val == null) {
				// $scope.datepickerObject.inputDate = previousDate;
			// } else {
				// $scope.datepickerObject.inputDate = new Date(val);
			// }
		},
		
	};
	var datePickerCallback = function(val) {
		if ( typeof (val) === 'undefined') {
			console.log('No date selected');
		} else {
			console.log('Selected date is : ', val);
			$ionicLoading.show({
			template : 'Loading...'
		});
		var year = $scope.datepickerObject.inputDate.getFullYear();
		var month = $scope.datepickerObject.inputDate.getMonth();
		var day = $scope.datepickerObject.inputDate.getDate();
		var date = (year + "-" + (month + 1) + "-" + day);

		var data = {
			"id" : JSON.parse(localStorage.getItem("user")).id,
			"date" : date
		};

		var request = Network.post(data, "add_leave");
		request.success(function(response) {
			console.log(response);
			$ionicLoading.hide();
			if (response.status == "success") {

				CommonCode.showAlert("Time log added successfully");
				$window.location.reload(true);
			}
		});
		request.error(function(response) {
			$ionicLoading.hide();
			CommonCode.showAlert(response.message);
		});

		}
	};
}).controller('SplashCtrl', function($scope, CommonCode, $rootScope) {
	console.log('splash');
	CommonCode.showHeader("", false);
	CommonCode.backButtonVisibility(false);
	CommonCode.logoutButtonVisibility("none");
	setTimeout(function() {
		if (localStorage.getItem("user") != undefined && localStorage.getItem("user") != "") {
			user_id = JSON.parse(localStorage.getItem("user")).id;
			console.log(user_id);
			location.href = '#/calendar';
		} else {
			location.href = '#/login';
		}

	}, 2000);
}).controller('LoginCtrl', function($scope, $ionicPopup, $state, CommonCode, UserService, $ionicLoading, Network, OnlineStatus, $rootScope, MESSAGE, $base64) {
	CommonCode.showHeader("Login", true);
	CommonCode.showLeaveButton(false);
	CommonCode.backButtonVisibility(false);
	CommonCode.logoutButtonVisibility("none");
	// Perform the login action when the user submits the login form
	$scope.doLogin = function(user) {
		$ionicLoading.show({
			template : 'Loading...'
		});
		console.log(user);
		if (user.name != "" && user.password != "") {
			//$scope.showAlert("Login Successful");

			var data = {
				"email" : user.name,
				"password" : $base64.encode(user.password),
				// "isGoogleAuthenticated" : 0
			};
			console.log(data);

			var request = Network.post(data, 'authenticate');

			request.success(function(response) {
				if (response.status === "success") {

					localStorage.setItem("user", JSON.stringify(response.data));
					user_id = response.data.id;
					user.name = undefined;
					user.password = undefined;
					$scope.userForm.$setPristine();

					$ionicLoading.hide();
					console.log("Login successful and opening info page");
					location.href = '#/calendar';

				} else {
					console.log(response);
					$ionicLoading.hide();
					CommonCode.showAlert(response.message);
				}

			});

			request.error(function(response) {
				console.log(response);
				$ionicLoading.hide();
			});

		}
		//location.href = '#/calendar';
	};

	$scope.googleSignIn = function() {
		$ionicLoading.show({
			template : 'Logging in...'
		});

		window.plugins.googleplus.login({}, function(user_data) {
			// For the purpose of this example I will store user data on local storage
			UserService.setUser({
				userID : user_data.userId,
				name : user_data.displayName,
				email : user_data.email,
				picture : user_data.imageUrl,
				accessToken : user_data.accessToken,
				idToken : user_data.idToken
			});

			//$state.go('timelog');
			console.log("UserData" + user_data.email);
			var data = {
				"email" : user_data.email,
				"password" : "",
				"google" : "1"
			};
			console.log(data);

			var request = Network.post(data, 'authenticate');

			request.success(function(response) {
				if (response.status === "success") {

					user_id = response.data.id;

					localStorage.setItem("user", JSON.stringify(response.data));

					$ionicLoading.hide();
					console.log("Login successful and opening info page");
					location.href = '#/calendar';

				} else {
					console.log(response);
					$ionicLoading.hide();
					CommonCode.showAlert(response.message);
				}

			});

			request.error(function(response) {
				console.log(response);
				$ionicLoading.hide();
			});

		}, function(msg) {
			$ionicLoading.hide();
			console.log("msg" + msg);
		});
	};

}).controller('ProjectsCtrl', function($scope, CommonCode, Network, OnlineStatus, $rootScope, $location, $ionicLoading, $ionicPlatform) {
	CommonCode.backButtonVisibility(true);
	CommonCode.showLeaveButton(false);
	CommonCode.logoutButtonVisibility("block");
	CommonCode.showHeader("Projects List", true);
	$scope.projects = [];
	$scope.projects = projects;
	$scope.setProjectTimelog = function(selectedProjectFromList) {
		$ionicLoading.show({
			template : 'Loading...'
		});
		selectedProject = selectedProjectFromList;
		var data = "";
		var request = Network.get(data, "categories");
		request.success(function(response) {
			console.log(response);
			var categoryCollection = [];
			categoryCollection = response.data;
			categories = [];
			angular.forEach(categoryCollection, function(value, key) {
				categories.push({
					name : value.name,
					id : value.id,
				});

			});
			console.log(projects);
			location.href = '#/timelog';
			$ionicLoading.hide();
		});
		request.error(function(response) {
			//location.href = '#/timelog';
			console.log(response);
			$ionicLoading.hide();
		});
	};

}).controller('CalendarCtrl', function($window, $scope, $ionicPopup, CommonCode, $rootScope, $location, $ionicLoading, Network, OnlineStatus, MESSAGE) {
	CommonCode.showLeaveButton(true);
	$scope.days = [];
	var monthCounter = 0;
	$ionicLoading.show({
		template : 'Loading...'
	});
	$scope.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var d = new Date();

	var getTimeLogs = function() {
		console.log("Time Logs : " + JSON.parse(localStorage.getItem("user")).id);
		var data = {
			"id" : JSON.parse(localStorage.getItem("user")).id,
			"start_date":d.getFullYear()+"-"+(d.getMonth()-2)+"-"+"01", 
			"end_date":d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate(),
			"limit":200
		};
		console.log()
		var request = Network.get(data, "time_logs");
		request.success(function(response) {

			console.log(response);
			var eventLogs = [];
			eventLogs = response.data;
			$scope.totalHrs = 0;
			var _prev = 0;
			angular.forEach(eventLogs, function(value, key) {
				if (value.description == "H") {
					console.log(value);
				}
				var _newDate = new Date(value.date);

				$scope.events.push({
					title : value.description == 'L' ? "<div id='" + value.id + "'>Applied leave for this day</div>" : value.description == 'H' ? "<div id='" + value.id + "'>" + value.holiday_description + "</div>" : "<div id='" + value.id + "'>" + value.hours + " Hrs : " + value.project + ":" + value.description + "</div>",
					type : 'success',
					startsAt : _newDate,
					editable : true,
					deletable : value.description == 'H' ? false : true,
					incrementsBadgeTotal : false,
					cssClass : 'eventBackground',
					hours : parseFloat(value.hours),
					description : value.description
				});

			});

			$ionicLoading.hide();
		});
		request.error(function(response) {
			$ionicLoading.hide();
			//location.href = '#/timelog';
			//console.log(response);
		});
	};
	getTimeLogs();

	var vm = $scope;

	vm.dateValue = $scope.monthNames[d.getMonth()];
	vm.calendarView = 'month';
	vm.calendarDay = new Date();
	vm.calendarTitle = new Date();
	vm.drillDown = function() {
		return false;
	};
	$scope.decrementMonth = function(calendarView) {
		console.log(vm);
		monthCounter--;
		console.log(monthCounter);
		console.log($scope.monthNames[((d.getMonth()) + monthCounter) % 12]);
		$scope.dateValue = $scope.monthNames[((d.getMonth()) + monthCounter) % 12];
	};
	$scope.incrementMonth = function() {

		monthCounter++;
		console.log(monthCounter);
		console.log($scope.monthNames[((d.getMonth()) + monthCounter)] % 12);
		
		$scope.dateValue = $scope.monthNames[((d.getMonth()) + monthCounter) % 12];
		
	};
	vm.eventDeleted = function(event) {
		//alert("Deleting event");
		//console.log(event);
		console.log(event.title.split("'")[1]);
		var confirmPopup = $ionicPopup.confirm({
			title : 'TimeLog',
			template : "Are you sure, you want to delete this log?"
		});
		confirmPopup.then(function(res) {
			if (res) {
				console.log('Yes');
				$ionicLoading.show({
					template : 'Loading...'
				});
				if (event.description != 'L') {
					var data = {
						"id" : JSON.parse(localStorage.getItem("user")).id,
						"time_log" : event.title.split("'")[1]
					};
					var request = Network.delete(data, "delete_time_log");
					request.success(function(response) {
						console.log(response);

						$ionicLoading.hide();
						$window.location.reload(true);
						//getTimeLogs();
					});
					request.error(function(response) {
						$ionicLoading.hide();

					});
				} else {
					var year = new Date(event.startsAt).getFullYear();
					var month = new Date(event.startsAt).getMonth();
					var day = new Date(event.startsAt).getDate();
					var date = (year + "-" + (month + 1) + "-" + day);

					var data = {
						"id" : JSON.parse(localStorage.getItem("user")).id,
						"date" : date
					};
					var request = Network.delete(data, "delete_leave");
					request.success(function(response) {
						console.log(response);

						$ionicLoading.hide();
						$window.location.reload(true);
						//getTimeLogs();
					});
					request.error(function(response) {
						$ionicLoading.hide();

					});
				}

			} else {
				console.log('No');
			}
		});

	};
	$scope.addTimeLog = function() {
		$ionicLoading.show({
			template : 'Loading...'
		});

		var data = {
			"id" : JSON.parse(localStorage.getItem("user")).id
		};
		var request = Network.get(data, "projects");
		request.success(function(response) {
			console.log(response);
			var projectCollection = [];
			projectCollection = response.data;
			projects = [];
			angular.forEach(projectCollection, function(value, key) {
				projects.push({
					name : value.name,
					id : value.id,
					client : value.client,
					progress : value.progress
				});

			});
			console.log(projects);
			location.href = '#/projects';
			$ionicLoading.hide();
		});
		request.error(function(response) {
			$ionicLoading.hide();
			if (message != "") {
				CommonCode.showAlert(response.message);
			}
			//location.href = '#/timelog';
			//console.log(response);
		});
		//projects = [{name:"YouDoBio",id:1},{name:"Kotak Bharat Banking",id:2}, {name:"Hirenetics",id:3}];
		//location.href = '#/projects';

	};
	$scope.doLogout = function() {
		CommonCode.logout();
	};

	CommonCode.showHeader("ADD TIMELOG", true);
	CommonCode.backButtonVisibility(false);
	CommonCode.logoutButtonVisibility("block");

}).controller('TimelogCtrl', function($scope, $ionicLoading, CommonCode, Network, OnlineStatus, $rootScope, $location) {
CommonCode.showLeaveButton(false);
	CommonCode.showHeader("Time Log", true);
	var hoursList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
	var minsList = [{
		name : "00 mins",
		value : 0
	}, {
		name : "15 mins",
		value : 25
	}, {
		name : "30 mins",
		value : 50
	}, {
		name : "45 mins",
		value : 75
	}];
	$scope.hours = hoursList;
	$scope.mins = minsList;

	$scope.projects = projects;
	$scope.categories = categories;
	$scope.selectedProject = selectedProject;
	$scope.selectedCategory = $scope.categories[0];
	$scope.selectedHours = $scope.hours[0];
	$scope.selectedMins = $scope.mins[0];

	var weekDaysList = ["Sun", "Mon", "Tue", "Wed", "thu", "Fri", "Sat"];
	var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	$scope.datepickerObject = {
		titleLabel : 'Select date', //Optional
		todayLabel : 'Today', //Optional
		closeLabel : 'Close', //Optional
		setLabel : 'Set', //Optional
		setButtonType : 'button-assertive', //Optional
		todayButtonType : 'button-assertive', //Optional
		closeButtonType : 'button-assertive', //Optional
		inputDate : new Date(), //Optional
		mondayFirst : true, //Optional
		disabledDates : '', //Optional
		weekDaysList : weekDaysList, //Optional
		monthList : monthList, //Optional
		templateType : 'popup', //Optional
		showTodayButton : 'false', //Optional

		modalHeaderColor : 'bar-positive', //Optional
		modalFooterColor : 'bar-positive', //Optional
		from : new Date(2015, 0, 1), //Optional
		to : new Date(), //Optional
		callback : function(val) {//Mandatory
			datePickerCallback(val);
			var previousDate = $scope.datepickerObject.inputDate;
			if (val == null) {
				$scope.datepickerObject.inputDate = previousDate;
			} else {
				$scope.datepickerObject.inputDate = new Date(val);
			}
		},
	};
	var datePickerCallback = function(val) {
		if ( typeof (val) === 'undefined') {
			console.log('No date selected');
		} else {
			console.log('Selected date is : ', val);

		}
	};
	$scope.saveTimeLog = function(addSimilar) {
		$ionicLoading.show({
			template : 'Loading...'
		});
		var year = $scope.datepickerObject.inputDate.getFullYear();
		var month = $scope.datepickerObject.inputDate.getMonth();
		var day = $scope.datepickerObject.inputDate.getDate();
		var date = (year + "-" + (month + 1) + "-" + day);

		var data = {
			"time_log" : {
				"user_id" : JSON.parse(localStorage.getItem("user")).id,
				"project_id" : $scope.selectedProject.id,
				"category_id" : $scope.selectedCategory.id,
				"date" : date,
				"hours" : $scope.selectedHours + "." + $scope.selectedMins.value,
				"description" : $scope.descriptionValue
			}
		};

		var request = Network.post(data, "add_time_log");
		request.success(function(response) {
			console.log(response);
			$ionicLoading.hide();
			if (response.status == "success") {

				CommonCode.showAlert("Time log added successfully");
				
				if (addSimilar) {
					$scope.descriptionValue = "";
					$scope.selectedProject = $scope.projects[0];
					$scope.selectedCategory = $scope.categories[0];
					$scope.selectedHours = $scope.hours[0];
					$scope.selectedMins = $scope.mins[0];
				}else{
					location.href = '#/calendar';
				}
			} else {
				$ionicLoading.hide();
				CommonCode.showAlert(response.message);
			}

		});
		request.error(function(response) {
			$ionicLoading.hide();
			console.log(response.status);
		});
	};

	$scope.applyLeave = function() {
		$ionicLoading.show({
			template : 'Loading...'
		});
		var year = $scope.datepickerObject.inputDate.getFullYear();
		var month = $scope.datepickerObject.inputDate.getMonth();
		var day = $scope.datepickerObject.inputDate.getDate();
		var date = (year + "-" + (month + 1) + "-" + day);

		var data = {
			"id" : JSON.parse(localStorage.getItem("user")).id,
			"date" : date
		};

		var request = Network.post(data, "add_leave");
		request.success(function(response) {
			console.log(response);
			$ionicLoading.hide();
			if (response.status == "success") {

				CommonCode.showAlert("Time log added successfully");
				location.href = '#/calendar';
			}
		});
		request.error(function(response) {
			$ionicLoading.hide();
			CommonCode.showAlert(response.message);
		});

	};

});
