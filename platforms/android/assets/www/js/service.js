angular.module('Timesheet.services', ['Timesheet.config']).service('UserService', function() {
	// For the purpose of this example I will store user data on ionic local storage but you should save it on a database

	var setUser = function(user_data) {
		window.localStorage.starter_google_user = JSON.stringify(user_data);
	};

	var getUser = function() {
		return JSON.parse(window.localStorage.starter_google_user || '{}');
	};

	return {
		getUser : getUser,
		setUser : setUser
	};
}).factory('Network', function($http, URL) {
	var self = this;

	self.post = function(data, url) {

		var request = $http({
			method : "POST",
			url : URL.base_value + url,
			headers : {
				"Content-Type" : "application/json",
				"Access-Control-Allow-Origin" : "*"
			},
			data : data
		});
		
		return request;
	};
	self.delete = function(data, url) {

		var request = $http({
			method : "DELETE",
			url : URL.base_value + url,
			headers : {
				"Content-Type" : "application/json",
				"Access-Control-Allow-Origin" : "*"
			},
			data : data
		});
		
		return request;
	};

	self.get = function(params, url) {

		var request = $http({
			method : "GET",
			url : URL.base_value + url,
			params : params
		});
		return request;
	};

	return self;

}).factory('OnlineStatus', ["$window", "$rootScope",
function($window, $rootScope, $cordovaNetwork) {
	var onlineStatus = {};

	onlineStatus.onLine = navigator.onLine;
	//$cordovaNetwork.isOnline();//isOnline();//

	onlineStatus.isOnline = function() {
		return onlineStatus.onLine;
	};

	$window.addEventListener("online", function() {
		onlineStatus.onLine = true;
		$rootScope.$digest();
	}, true);

	$window.addEventListener("offline", function() {
		onlineStatus.onLine = false;
		$rootScope.$digest();
	}, true);

	return onlineStatus;
}]);
